# git-with-ssh

This image just has git and SSH so I can trigger a commit to another repo after a docker build completes, like this:

```
build:
  image: docker:19.03.1
  services:
    - docker:19.03.1-dind
  variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
  before_script:
    - 'docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY'
  stage: build
  script:
    - 'docker pull $CI_REGISTRY_IMAGE:latest || true'
    - 'docker build --cache-from $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME .'
    - 'docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA'
    - 'docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME'

deploy:
  image: registry.gitlab.com/dedevsecops/git-with-ssh:latest
  stage: deploy
  before_script:
    - 'eval $(ssh-agent -s)'
    - 'echo "$SSH_PRIVATE_KEY" | tr -d "\r" | ssh-add -'
    - 'mkdir -p ~/.ssh'
    - 'chmod 700 ~/.ssh'
    - 'ssh-keyscan gitlab.com >> ~/.ssh/known_hosts'
    - 'chmod 644 ~/.ssh/known_hosts'
  script:
    - 'git clone git@gitlab.com:dedevsecops/someotherrepo.git'
    - 'cd someotherrepo'
    - 'sed -i "s/^  tag:.*/  tag: $CI_COMMIT_SHA/" helmfile.d/values/tenant/app.yaml'
    - 'git diff'
    - 'git add helmfile.d/values/tenant/app.yaml'
    - 'git status'
    - 'git commit -m "app gitlab-ci updating image tag."'
    - 'git push'
```
